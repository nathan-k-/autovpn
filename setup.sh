## Install scaleway cli ##
export ARCH=amd64  # can be 'i386', 'amd64' or 'armhf'
wget "https://github.com/scaleway/scaleway-cli/releases/download/v1.20/scw_1.20_${ARCH}.deb" -O /tmp/scw.deb
sudo dpkg -i /tmp/scw.deb && rm -f /tmp/scw.deb

# test
scw version

## Setup env ##
source ./secrets.sh # contains SCW_TOKEN
VM_NAME=ovpn
scw login -s -o $SCW_ORG  -t $SCW_TOKEN

## Create a vps ##
scw create --name=$VM_NAME --commercial-type=DEV1-S CentOS_7_6
scw -q start --timeout=300 --wait=true $VM_NAME
rm -f ~/.scw-cache.db
export SERVER_IP=$(scw inspect -f "{{ .PublicAddress.IP }}" server:$VM_NAME)

# Remove previous openvpn config file
rm -rf /tmp/personnalvpn.ovpn

sed -i "s/SERVER_IP/$SERVER_IP/g" ./inventories/vps.conf
ansible-playbook -v -i ./inventories/vps.conf ./openvpn.playbook.yml

cp /tmp/personnalvpn.ovpn ~/.vpn/
# And you're good to go !